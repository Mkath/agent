package com.codev.agente;

import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import com.codev.apiagente.Percepcion;

public class MainActivity extends AppCompatActivity {

    private TextView tvHello;
    private EditText etName;

    private ConstraintLayout constraintLayout;


    private Percepcion percepcion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvHello = findViewById(R.id.tvHello);
        etName = findViewById(R.id.etName);
        constraintLayout = findViewById(R.id.action_container);

        percepcion = new Percepcion();


        etName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                tvHello.setText("Hola!, ¿Cómo te llamas?");
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tvHello.setText("Hola!");

            }

            @Override
            public void afterTextChanged(Editable s) {

                if(etName.getText().toString().length() != 0){
                    tvHello.setText(percepcion.setName(etName.getText().toString()));
                }else{
                    tvHello.setText("Hola!");
                }
            }
        });

    }
}
